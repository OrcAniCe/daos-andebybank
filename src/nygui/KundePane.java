package nygui;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
//import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class KundePane extends GridPane {

	// private ListView<Transaktion> lstTransaktion = new ListView<>();
	private TextField txfCpr = new TextField();
	private TextField txfNavn = new TextField();
	private TextField txfAdresse = new TextField();
	private TextField txfPostNr = new TextField();
	private TextField txfBy = new TextField();
	private Button btnOpret = new Button("Opret Kunde");
	private Label lblError;

	public KundePane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		lblError = new Label();
		lblError.setTextFill(Color.RED);

		// TextField Cpr
		txfCpr = new TextField();
		txfCpr.setPrefWidth(60);
		txfCpr.setEditable(true);
		txfCpr.setText("CPR-Nummer");
		this.add(txfCpr, 2, 1);
		// TextField Navn
		txfNavn = new TextField();
		txfNavn.setPrefWidth(60);
		txfNavn.setEditable(true);
		txfNavn.setText("Navn");
		this.add(txfNavn, 2, 2);
		// TextField Adresse
		txfAdresse = new TextField();
		txfAdresse.setPrefWidth(60);
		txfAdresse.setEditable(true);
		txfAdresse.setText("Adresse");
		this.add(txfAdresse, 2, 3);
		// TextField Adresse
		txfPostNr = new TextField();
		txfPostNr.setPrefWidth(60);
		txfPostNr.setEditable(true);
		txfPostNr.setText("PostNr");
		this.add(txfPostNr, 2, 4);
		// TextField Adresse
		txfBy = new TextField();
		txfBy.setPrefWidth(60);
		txfBy.setEditable(true);
		txfBy.setText("By");
		this.add(txfBy, 3, 4);
		// Button Opret
		this.add(btnOpret, 2, 5);
		btnOpret.setOnAction(event -> actionOpret());
	}

	public void actionOpret() {

	}
}