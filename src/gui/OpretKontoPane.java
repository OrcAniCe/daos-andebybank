package gui;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Kunde;

public class OpretKontoPane extends GridPane {

	private Button btnAddHold;
	private Button btnEditHold;
	private Label lblEks;
	private TextField txfIdInput;
	private ListView<String> lvwEks;
	private Kunde kunde;
	private Label errorMsg;
	ArrayList<String> holdliste = new ArrayList<>();

	public OpretKundePane() {

		setPadding(new Insets(20));
		setHgap(20);
		setVgap(10);
		setGridLinesVisible(false);

		errorMsg = new Label();
		this.add(errorMsg, 0, 4);

		lblEks = new Label("Eksempel hold:");
		this.add(lblEks, 0, 0);

		lvwEks = new ListView<>();
		this.add(lvwEks, 0, 1, 2, 2);
		lvwEks.setPrefWidth(200);
		lvwEks.setPrefHeight(100);

		txfIdInput = new TextField();
		txfIdInput.setPrefWidth(150);
		txfIdInput.setEditable(true);
		this.add(txfIdInput, 1, 3);

		btnAddHold = new Button("Add");
		this.add(btnAddHold, 0, 3);
		btnAddHold.setOnAction(event -> eksMetode());

		btnEditHold = new Button("Edit");
		this.add(btnEditHold, 0, 3);
		btnEditHold.setOnAction(event -> eksMetode());

	}

	public void updateControls() {
		try {
			SQLcon sql = new SQLcon();
			sql.open(Connection.TRANSACTION_SERIALIZABLE);

			ResultSet res = sql.query("SELECT * From Hold");
			while (res.next()) {
				holdliste.add(res.getString("Navn"));
				lvwEks.getItems().setAll(holdliste);
			}
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
	}

	private void eksMetode() {

		String inputId = txfIdInput.getText();
		if (!hold.getMedlem(txfIdInput.getText())) {
			this.errorMsg.setText("Error during creation");
		}

	}

}
