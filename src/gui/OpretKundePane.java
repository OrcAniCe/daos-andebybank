package gui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class OpretKundePane extends GridPane {
	String dbURL = "jdbc:sqlserver://mssql4.gear.host";
	String user = "skole2";
	String pass = "Lr3CUV7648~~";

	private Label lblEks;
	private TextField txfNavn;
	private TextField txfAdresse;
	private TextField txfTlf;
	private ListView<String> lvwEks;
	ArrayList<String> liste = new ArrayList<>();

	public OpretKundePane() {

		setPadding(new Insets(20));
		setHgap(20);
		setVgap(10);
		setGridLinesVisible(false);

		lblEks = new Label("Eksempel Medlem:");
		this.add(lblEks, 0, 0);

		lvwEks = new ListView<>();
		this.add(lvwEks, 0, 1, 2, 2);
		lvwEks.setPrefWidth(200);
		lvwEks.setPrefHeight(100);

		txfNavn = new TextField();
		txfNavn.setPrefWidth(150);
		txfNavn.setEditable(true);
		this.add(txfNavn, 1, 3);

		txfAdresse = new TextField();
		txfAdresse.setPrefWidth(150);
		txfAdresse.setEditable(true);
		this.add(txfAdresse, 2, 3);

		txfTlf = new TextField();
		txfTlf.setPrefWidth(150);
		txfTlf.setEditable(true);
		this.add(txfTlf, 3, 3);

	}

	public void updateControls() {
		try {
			Connection minConnection;
			String dbURL = "jdbc:sqlserver://mssql4.gear.host";
			String user = "skole2";
			String pass = "Lr3CUV7648~~";
			minConnection = DriverManager.getConnection(dbURL, user, pass);
			Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet res = stmt.executeQuery("SELECT * From Kunde");
			while (res.next()) {
				liste.add(res.getString("Navn"));
				lvwEks.getItems().setAll(liste);
			}
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
	}

}