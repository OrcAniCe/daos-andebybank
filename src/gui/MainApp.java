package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		primaryStage.setTitle("AndeBy Bank");
		primaryStage.setWidth(800);
		primaryStage.setHeight(800);
		primaryStage.setResizable(false);
		GridPane pane = new GridPane();
		pane.setMinSize(600, 600);
		pane.setGridLinesVisible(false);


		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private BorderPane initContent() {
		BorderPane pane = new BorderPane();

		TabPane tabPane = initTabPane();
		pane.setCenter(tabPane);

		return pane;
	}

	private TabPane initTabPane() {
		TabPane tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		// Tab Hold
		Tab tabKunde = new Tab("Opret Kunde");
		tabPane.getTabs().add(tabKunde);

		OpretKundePane kundePane = new OpretKundePane();
		tabKunde.setContent(kundePane);
		tabKunde.setOnSelectionChanged(event -> kundePane.updateControls());

		// Tab Hold
		Tab tabKunde2 = new Tab("Opret Kunde");
		tabPane.getTabs().add(tabKunde2);

		OpretKundePane kundePane2 = new OpretKundePane();
		tabKunde.setContent(kundePane2);
		tabKunde.setOnSelectionChanged(event -> kundePane2.updateControls());
		return tabPane;
	}

}