package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Kunde {
	// CprNr
	private final StringProperty cprNr = new SimpleStringProperty(this, "cprNr");

	public StringProperty cprNrProperty() {
		return cprNr;
	}

	public final String getCprNr() {
		return cprNrProperty().get();
	}

	public final void setCprNr(String cprNr) {
		cprNrProperty().set(cprNr);
	}

	// Navn
	private final StringProperty navn = new SimpleStringProperty(this, "navn");

	public StringProperty navnProperty() {
		return navn;
	}

	public final String getNavn() {
		return navnProperty().get();
	}

	public final void setNavn(String navn) {
		navnProperty().set(navn);
	}

	// Adresse
	private final StringProperty adresse = new SimpleStringProperty(this, "adresse");

	public StringProperty adresseProperty() {
		return adresse;
	}

	public final String getAdresse() {
		return adresseProperty().get();
	}

	public final void setAdresse(String adresse) {
		adresseProperty().set(adresse);
	}

	// PostNr
	private final IntegerProperty postNr = new SimpleIntegerProperty(this, "postNr");

	public IntegerProperty postNrProperty() {
		return postNr;
	}

	public final int getPostNr() {
		return postNrProperty().get();
	}

	public final void setPostNr(int postNr) {
		postNrProperty().set(postNr);
	}

	// Stuff
	public Kunde(String cprNr, String navn, String adresse, int postNr) {
		setCprNr(cprNr);
		setNavn(navn);
		setAdresse(adresse);
		setPostNr(postNr);
	}

}